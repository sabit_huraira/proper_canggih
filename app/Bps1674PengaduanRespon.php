<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bps1674PengaduanRespon extends Model
{
    protected $table = 'bps1674_pengaduan_respon';
    
    public function attributes()
    {
        return (new \App\Http\Requests\Bps1674PengaduanResponRequest())->attributes();
    }
    
    public function CreatedUser()
    {
        return $this->hasOne('App\User', 'id', 'created_by');
    }
}
