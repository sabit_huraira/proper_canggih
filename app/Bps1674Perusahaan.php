<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bps1674Perusahaan extends Model
{
    protected $table = 'bps1674_perusahaan';
    
    public function attributes()
    {
        return (new \App\Http\Requests\Bps1674PerusahaanRequest())->attributes();
    }
    
    public function getListBadanUsahaAttribute(){
        return [
            1 => "Tidak Berbadan Hukum",
            2 => "CV",
            3 => "PT",
            4 => "Lainnya",
        ];
    }
    
    public function getListKategoriAttribute(){
        return [
            "A" => "Pertanian, Kehutanan, dan Perikanan",
            "B" => "Pertambangan dan Penggalian ",
            "C" => "Industri Pengolahan",
            "D" => "Pengadaan Listrik, Gas, Uap/Air Panas dan Udara Dingin",
            "E" => "Pengadaan Air, Pengelolaan Sampah dan Daur Ulang,Pembuangan dan Pembersihan Limbah dan Sampah",
            "F" => "Konstruksi",
            "G" => "Perdagangan Besar dan Eceran; Reparasi dan Perawatan Mobil dan Sepeda Motor",
            "H" => "Transportasi dan Pergudangan",
            "I" => "Penyediaan Akomodasi dan Penyediaan Makan Minum",
            "J" => "Informasi dan Komunikasi",
            "K" => "Jasa Keuangan dan Asuransi",
            "L" => "Real Estate",
            "M" => "Jasa Profesional, Ilmiah dan Teknis",
            "N" => "Jasa Persewaan & Sewa Guna Usaha Tanpa Hak Opsi, Ketenagakerjaan, Agen Perjalanan dan Penunjang Usaha Lainnya",
            "O" => "Administrasi Pemerintahan, Pertahanan dan Jaminan Sosial Wajib",
            "P" => "Jasa Pendidikan",
            "Q" => "Jasa Kesehatan dan Kegiatan Sosial",
            "R" => "Kesenian, Hiburan dan Rekreasi",
            "S" => "Kegiatan Jasa Lainnya",
            "T" => "Jasa Perorangan yg Melayani Rumah Tangga; Kegiatan yg Menghasilkan Barang dan Jasa Oleh Rumah Tangga yg Digunakan Sendiri Untuk Memenuhi Kebutuhan",
            "U" => "Kegiatan Badan Internasional dan Badan Ekstra Internasional Lainnya",
        ];
    }

    public function getListPeriodeSurveyAttribute(){
        return [
            1 => "Bulanan",
            2 => "Triwulan",
            3 => "Semester",
            4 => "Tahunan",
            0 => "Lainnya",
        ];
    }
}
