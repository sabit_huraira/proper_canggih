<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Bps1674PerusahaanRequest;
use App\Http\Requests\Bps1674PerusahaanKunjunganRequest;
use App\Http\Requests\Bps1674PerusahaanSurveyRequest;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('data.index');
    }

    public function publikasi(Request $request)
    {
        return view('data.publikasi');
    }

    public function berita(Request $request)
    {
        return view('data.berita');
    }

    public function dinamis(Request $request)
    {
        return view('data.dinamis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
    }
}
