<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Bps1674PerusahaanKunjunganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'tanggal_diserahkan' => 'required',
            'status_dokumen' => 'required',
            // '' => 'required',
            // 'kategori' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'perusahaan_survey_id' => 'Perusahaan',
            'nama_pengantar' => 'Nama Pengantar',
            'nama_penerima' => 'Nama Penerima',
            'tanggal_diserahkan' => 'Tanggal diserahkan',
            'tanggal_dikembalikan' => 'Tanggal dikembalikan',
            'status_dokumen' => 'Status dokumen',
            'keterangan' => 'Keterangan',
            'created_by' => 'Dibuat oleh',
            'updated_by' => 'Terkahir diperbaharui oleh',
            'created_at' => 'Dibuat pada',
            'updated_at' => 'Terakhir diperbaharui pada',
        ];
    }
}
