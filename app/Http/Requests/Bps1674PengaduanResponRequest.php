<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Bps1674PengaduanResponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    public function rules()
    {
        return [
            'isi' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'isi' => 'Isi',
            'created_by' => 'Dibuat oleh',
            'updated_by' => 'Terkahir diperbaharui oleh',
            'created_at' => 'Dibuat pada',
            'updated_at' => 'Terakhir diperbaharui pada',
        ];
    }
}