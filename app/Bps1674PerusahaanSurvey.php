<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bps1674PerusahaanSurvey extends Model
{
    protected $table = 'bps1674_perusahaan_survey';
    protected $appends = array('kunjungan');
    
    public function attributes()
    {
        return (new \App\Http\Requests\Bps1674PerusahaanSurveyRequest())->attributes();
    }

    public function getKunjunganAttribute()
    {
        // return $this->hasMany('App\Bps1674PerusahaanKunjungan');
        $datas = \App\Bps1674PerusahaanKunjungan::where('perusahaan_survey_id', '=', $this->id)->get();
        return $datas;
    }
}
