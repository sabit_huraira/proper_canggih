<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('testa', function () {
    // print_r(Flysystem::connection('webdav')->listContents('remote.php/webdav/'));
    
    $data = Flysystem::connection('webdav')->put('test.txt', 'hai hai');
    print_r("udahan");
    die();
});

Route::group(['middleware' => ['role:superadmin']], function () {    
    Route::resource('user','UserController');

    //SPA
    Route::resource('role','RoleController');

    Route::resource('user_role','UserRoleController');
    Route::get('pengaduan/{id}/create_respon','PengaduanController@create_respon');
    Route::post('pengaduan/{id}/store_respon','PengaduanController@store_respon');
});


Route::group(['middleware' => 'auth'], function(){
    Route::resource('pengaduan','PengaduanController')->except(['show']);
    Route::get('pengaduan/{id}/detail','PengaduanController@detail');
    
});

Auth::routes();

Route::get('/', 'DataController@index')->name('home');
Route::get('guest', 'HomeController@guest')->name('guest');

Route::get('data','DataController@index');
Route::get('data/index','DataController@index');
Route::get('data/publikasi','DataController@publikasi');
Route::get('data/berita/','DataController@berita');
Route::get('data/dinamis/','DataController@dinamis');
