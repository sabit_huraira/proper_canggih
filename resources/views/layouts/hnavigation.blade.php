<div class="main_menu">
    <nav class="navbar navbar-expand-lg">
        <div class="container">

            <div class="navbar-collapse align-items-center collapse" id="navbar">
                <ul class="navbar-nav">
                    <li class="nav-item {{ request()->is('data/index*') ? 'active' : '' }}">
                        <a href="{{ url('data/index') }}" class="nav-link">
                            Tabel Statis
                        </a>
                    </li>
                
                    
                    <li class="nav-item {{ request()->is('data/publikasi*') ? 'active' : '' }}">
                        <a href="{{ url('data/publikasi') }}" class="nav-link">
                            Publikasi
                        </a>
                    </li>
                    
                    <li class="nav-item {{ request()->is('data/berita*') ? 'active' : '' }}">
                        <a href="{{ url('data/berita') }}" class="nav-link">
                            Berita
                        </a>
                    </li>
                    
                    <li class="nav-item {{ request()->is('pengaduan*') ? 'active' : '' }}">
                        <a href="{{ url('pengaduan') }}" class="nav-link">
                            Pengaduan
                        </a>
                    </li>
                    

                    @if (Auth::check())
                        @if(auth()->user()->hasRole('superadmin'))
                            <li class="nav-item {{ request()->is('user_role*') ? 'active' : '' }}">
                                <a href="{{ url('user_role') }}" class="nav-link">
                                    Pengaturan Level User
                                </a>
                            </li>
                        @endif
                    @endif
                
                    <li class="nav-item">
                        @if (Auth::check())
                            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                Logout ({{ Auth::user()->name }})
                            </a>
                            
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        @else
                            <a href="{{url('login')}}" class="nav-link"><i class="icon-login"></i> Login</a>
                        @endif
                    </li>
                </ul>
            </div>

        </div>
    </nav>
</div>