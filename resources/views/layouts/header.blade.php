
    <nav class="navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-brand">
                <a href="#">
                    <img src="{!! asset('lucid/assets/images/daster.png') !!}" alt="Teras Logo" style="width:30px" class="img-responsive logo">
                </a>            
            </div>
            
            
            <div class="navbar-right">              

                <div id="navbar-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-settings"></i> Aplikasi BPS</a>
                            <ul class="dropdown-menu user-menu menu-icon">
                                <li><a href="https://silastik.bps.go.id"><span>SILASTIK</span></a></li>
                                <li><a href="https://perpustakaan.bps.go.id"><span>DIGILIB</span></a></li>
                                <li><a href="https://mikrodata.bps.go.id"><span>Katalog Mikro Data</span></a></li>
                                <li><a href="https://sirusa.bps.go.id"><span>SIRUSA</span></a></li>
                            </ul>
                        </li>
                        
                    </ul>
                </div>
            </div>

            <div class="navbar-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                    <i class="lnr lnr-menu fa fa-bars"></i>
                </button>
            </div>
        </div>
    </nav>