@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('data')}}"><i class="icon-home"></i></a></li>                     
    <li class="breadcrumb-item">Data BPS -  Berita</li>
</ul>
@endsection

@section('content')
    <div class="container"  id="app_vue">
      <br />
      @if (\Session::has('success'))
        <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
        </div><br />
      @endif

      <div class="card">
        <div class="body">
            
            <div class="input-group mb-3"> 
                <input type="text" class="form-control" name="search" v-model="keyword" placeholder="Search.." v-on:keydown.enter="setDatas">
                <div class="input-group-append">
                    <button class="btn btn-info" v-on:click="setDatas"><i class="fa fa-search"></i></button>
                </div>
            </div>
            <p class="text-muted float-left">Ditemukan @{{ total_data }} data</p>
            <u><a class="float-right" href="{{action('DataController@berita')}}">reset pencarian</a></u>
            <br/><br/>
          <div>@include('data.list_berita')</div>
      </div>
    </div>

      <div class="modal hide" id="wait_progres" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-body">
                      <div class="text-center"><img src="{!! asset('lucid/assets/images/loading.gif') !!}" width="200" height="200" alt="Loading..."></div>
                      <h4 class="text-center">Please wait...</h4>
                  </div>
              </div>
          </div>
      </div>

  </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>
    <script src="{!! asset('lucid/assets/vendor/pagination/twbs-pagination-master/jquery.twbsPagination.min.js') !!}"></script>

    <script>
        var vm = new Vue({
            el: "#app_vue",
            data:  {
                keyword: '',
                berita: [],
                page: 1,
                total_data: 0,
                pages: 0,
                api_path: 'https://webapi.bps.go.id/v1/api/list',
                api_key: '9a1097f6a7994990a7a8db0e463615ce',
                domain: '1674',
            },
            methods: {
                setDatas: function(){
                    var self = this;
                    $('#wait_progres').modal('show');

                    //berita
                    $.ajax({
                        url : self.api_path, method : 'get', dataType: 'json',
                        data:{
                            model: 'news', lang: 'ind', domain: self.domain,
                            key: self.api_key, keyword: self.keyword,
                        },
                    }).done(function (data) {

                        if(data["data-availability"]=="available"){
                            self.berita = data.data[1]; 
                            self.total_data = data.data[0].total;
                            self.pages = data.data[0].pages;
                        }
                        else{
                            alert("data tidak ditemukan");
                            self.berita = [];
                            self.total_data = 0;
                        }

                        $('#pagination-demo').twbsPagination('destroy');
                        $('#pagination-demo').twbsPagination($.extend({}, 
                            {
                                totalPages: 1,
                                visiblePages: 5,
                            }, 
                            {
                                totalPages: self.pages,
                                startPage: 1,
                                onPageClick: function (event, page) {
                                    self.page = page;
                                    self.setPage();
                                }
                            }
                        ));
                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg)); $('#wait_progres').modal('hide');
                    });
                },
                setPage: function(){
                    var self = this;
                    $('#wait_progres').modal('show');
                    
                    //table_statis
                    $.ajax({
                        url : self.api_path, method : 'get', dataType: 'json',
                        data:{
                            model: 'news', lang: 'ind', domain: self.domain,
                            key: self.api_key, keyword: self.keyword,
                            page: self.page,
                        },
                    }).done(function (data) {

                        if(data["data-availability"]=="available"){
                            self.berita = data.data[1]; 
                            self.total_data = data.data[0].total;
                        }
                        else{
                            // alert("data tidak ditemukan");
                            self.berita = [];
                            self.total_data = 0;
                        }


                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg)); $('#wait_progres').modal('hide');
                    });
                },
                urlBerita: function(judul, tanggal, id){
                    var hasil = "https://lubuklinggaukota.bps.go.id/news/";
                    return hasil.concat(
                        tanggal.split("-").join("/"),
                        "/",
                        id,
                        "/",
                        judul.split(" ").join("-").toLowerCase(),
                        ".html"
                    );
                },
            }
        });

        $(document).ready(function() {
            vm.setDatas();
            $('#pagination-demo').twbsPagination({
                totalPages: 1,
                visiblePages: 5,
            });
        });

    </script>
@endsection
