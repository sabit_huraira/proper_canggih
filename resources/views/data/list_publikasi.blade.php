
<div id="publikasi">
    <div class="table-responsive">
        <table class="table-bordered m-b-0" style="min-width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th class="text-center">Judul</th>
                    <th class="text-center">Tanggal Rilis</th>
                    <th class="text-center">Unduh</th>
                </tr>
            </thead>

            <tbody>
                <tr v-for="(data, index) in publikasi" :key="data.pub_id">
                    <td>@{{ index+1 }}</td>
                    <td>
                        @{{ data.title }}<br/>
                        <small class="text-muted">(ISSN: @{{ data.issn }})</small>
                    </td>
                    <td class="text-center">@{{ data.rl_date }}</td>
                    <td class="text-center">
                        <a :href="data.pdf">Unduh</a> <small class="text-muted">(size: @{{ data.size }})</small>
                        <img :src="'http://www.barcodes4.me/barcode/qr/bps.png?value='+data.pdf+'&ecclevel=0'"></img> 
                    </td>
                </tr>
            </tbody>
        </table>
        
        <br/>
        <ul id="pagination-demo" class="pagination-sm"></ul>
    </div>
</div>