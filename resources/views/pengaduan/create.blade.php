@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('data')}}"><i class="icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{url('pengaduan')}}">Pengaduan</a></li>                            
    <li class="breadcrumb-item">Tambah Pengaduan</li>
</ul>
@endsection

@section('content')
<div class="row clearfix">
  <div class="col-md-12">
      <div class="card">
          <div class="header">
              <h2>Tambah Pengaduan</h2>
          </div>
          <div class="body">
              <form method="post" action="{{url('pengaduan')}}" enctype="multipart/form-data">
              @csrf
              @include('pengaduan._form')
              </form>
          </div>
      </div>
  </div>
</div>
@endsection
