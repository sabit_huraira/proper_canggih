@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('data')}}"><i class="icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{url('pengaduan')}}">Pengaduan</a></li>
</ul>
@endsection

@section('content')
<div class="row clearfix">
  <div class="col-md-12">
      <div class="card">
            <div class="body">
                
                @if (Auth::check())
                    @if(auth()->user()->hasRole('superadmin'))
                        <a href="{{ action('PengaduanController@create_respon', $model->id) }}" class="btn btn-info">Tambah Balasan</a>
                        <hr/>
                    @endif
                @endif
                
                <div date-is="20-04-2018 - Today">
                    <h5>{{ $model->judul }}</h5>
                    <span><a href="#">{{ $model->createdUser->name }}</a> <small class="float-right">{{ $model->created_at->diffForHumans() }}</small></span>
                    <div class="msg">
                        <p>{{ $model->isi }}</p>
                    </div>                                
                </div>

                <hr>

                <ul class="right_chat list-unstyled mb-0">
                    @foreach($datas as $key=>$data)
                    <li class="offline">
                        <a href="javascript:void(0);">
                            <div class="media">
                                <div class="media-body">
                                    <span class="name">{{ $data->createdUser->name }} <small class="float-right">{{ $data->created_at->diffForHumans() }}</small></span>
                                    <span>{{ $data->isi }}</span>
                                </div>
                            </div>
                        </a>                            
                    </li>  
                    @endforeach               
                </ul>                            
            </div>
        </div>
  </div>
</div>
@endsection


@section('css')
    <meta name="_token" content="{{csrf_token()}}" />
    <meta name="csrf-token" content="@csrf">
    <link rel="stylesheet" href="{!! asset('lucid/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}">
@endsection
