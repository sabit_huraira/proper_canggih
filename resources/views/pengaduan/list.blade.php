<div id="load" class="table-responsive">
    <table class="table-bordered m-b-0" style="min-width:100%">
        @if (count($datas)==0)
            <thead>
                <tr><th>Tidak ditemukan data</th></tr>
            </thead>
        @else
           
            <tbody>
                @foreach($datas as $data)
                <tr>
                    <td>
                        <h6 class="margin-0 pt-1" style="wrap-text: true">
                            <p class="badge badge-info">{{ date('d F Y', strtotime($data['created_at'])) }}</p>
                        
                            <a href="{{action('PengaduanController@detail', $data->id)}}">{{$data['judul']}}</a>
                        </h6>
                        <p class="text-muted pl-2">{{ $data->createdUser->name }}</p>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        @endif
    </table>
    {{ $datas->links() }} 
</div>