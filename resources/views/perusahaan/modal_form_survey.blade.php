<div class="modal fade" id="form_survey" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <b>Form Survey</b>
            </div>
            <div class="modal-body">
                <input type="hidden" v-model="f_id">

                <div class="row clearfix">
                    <div class="col-md-12">
                        Nama Survey:
                        <div class="form-line">
                            <input type="text" v-model="f_nama_survey" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        Jumlah Pegawai:
                        <div class="form-line">
                            <input type="text" v-model="f_jumlah_pegawai" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Periode Pencacahan:
                        <div class="form-line">
                            <select class="form-control" v-model="f_periode_pencacahan">
                                @foreach ($model->ListPeriodeSurvey as $key=>$value)
                                    <option value="{{ $key }}">{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div v-if="f_periode_pencacahan == 1">Bulan</div>
                        <div v-else-if="f_periode_pencacahan == 2">Triwulan</div>
                        <div v-else-if="f_periode_pencacahan == 3">Semester</div>


                        <div v-if="f_periode_pencacahan != 4 && f_periode_pencacahan !=0" class="form-line">
                            <input type="text" v-model="f_month" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Tahun:
                        <div class="form-line">
                            <input type="text" v-model="f_year" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        Nama Petugas:
                        <div class="form-line">
                            <input type="text" v-model="f_nama_petugas" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        HP Petugas:
                        <div class="form-line">
                            <input type="text" v-model="f_hp_petugas" class="form-control">
                        </div>
                    </div>
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-6">
                        Pemberi Jawaban:
                        <div class="form-line">
                            <input type="text" v-model="f_pemberi_jawaban" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Kegiatan Utama Perusahaan:
                        <div class="form-line">
                            <input type="text" v-model="f_kegiatan_utama" class="form-control">
                        </div>
                    </div>
                </div>
                
                <div class="row clearfix">
                    <div class="col-md-6">
                        Produk Utama:
                        <div class="form-line">
                            <input type="text" v-model="f_produk_utama" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Status Usaha:
                        <div class="form-line">
                            <select class="form-control" v-model="f_status_usaha" >
                                <option value="1">Aktif</option>
                                <option value="2">Tidak Aktif</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        Pendapatan:
                        <div class="form-line">
                            <input type="text" v-model="f_pendapatan" class="form-control">
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        Pengeluaran:
                        <div class="form-line">
                            <input type="text" v-model="f_pengeluaran" class="form-control">
                        </div>
                    </div>
                </div>

                
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" v-on:click="saveSurvey">SAVE</button>
                <button type="button" class="btn btn-simple" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>