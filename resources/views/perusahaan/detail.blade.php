@extends('layouts.admin')

@section('breadcrumb')
<ul class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{url('/')}}"><i class="icon-home"></i></a></li>
    <li class="breadcrumb-item"><a href="{{url('perusahaan')}}">Perusahaan</a></li>                            
    <li class="breadcrumb-item">{{ $model->nama }}</li>
</ul>
@endsection

@section('content')
<div class="row clearfix">
  <div class="col-md-12">
      <div class="card">
          <div class="header">
              <h2>Perusahaan {{ $model->nama }}</h2>
          </div>
          <div class="body">
          <div id="app_vue">
                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['nama']}}</label>
                            <p>{{ $model->nama }}</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['kategori']}}</label>
                            <select class="form-control" name="kategori" disabled>
                                @foreach ($model->ListKategori as $key=>$value)
                                    <option value="{{ $key }}" 
                                        @if ($key == old('kategori', $model->kategori))
                                            selected="selected"
                                        @endif >{{ $key }} - {{ $value }}</option>
                                @endforeach
                            </select>
                           
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['deskripsi']}}</label>
                            <textarea class="form-control {{($errors->first('deskripsi') ? ' parsley-error' : '')}}" name="deskripsi" rows="6" disabled>{{ old('deskripsi', $model->deskripsi) }}</textarea>
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['alamat']}}</label>
                            <textarea class="form-control {{($errors->first('alamat') ? ' parsley-error' : '')}}" name="alamat" rows="6" disabled>{{ old('alamat', $model->alamat) }}</textarea>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ $model->attributes()['kdprop']}}</label>
                            <p>{{ $model->kdprop }}</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ $model->attributes()['kdkab']}}</label>
                            <p>{{ $model->kdkab }}</p>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label>{{ $model->attributes()['kdkec']}}</label>
                            <p>{{ $model->kdkec }}</p>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['desa']}}</label>
                            <p>{{ $model->desa }}</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['telepon']}}</label>
                            <p>{{ $model->telepon }}</p>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label><span style="color: red; display:block; float:right">*</span>{{ $model->attributes()['badan_usaha']}}</label>
                            <select class="form-control" name="badan_usaha" disabled>
                                @foreach ($model->ListBadanUsaha as $key=>$value)
                                    <option value="{{ $key }}" 
                                        @if ($key == old('badan_usaha', $model->badan_usaha))
                                            selected="selected"
                                        @endif >{{ $value }}</option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['tahun_mulai']}}</label>
                            <p>{{ $model->tahun_mulai }}</p>
                        </div>
                    </div>
                </div>

                <div class="row clearfix">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['longitude']}}</label>
                            <p>{{ $model->longitude }}</p>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ $model->attributes()['lat']}}</label>
                            <p>{{ $model->lat }}</p>
                        </div>
                    </div>
                </div>

                @include('perusahaan.list_survey_detail')
                @include('perusahaan.modal_form_survey')
                @include('perusahaan.modal_form_kunjungan')
                <input type="hidden" name="total_utama" v-model="total_utama">


                <div class="modal hide" id="wait_progres" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="text-center"><img src="{!! asset('lucid/assets/images/loading.gif') !!}" width="200" height="200" alt="Loading..."></div>
                                <h4 class="text-center">Please wait...</h4>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

          </div>
      </div>
  </div>
</div>
@endsection


@section('css')
    <meta name="_token" content="{{csrf_token()}}" />
    <meta name="csrf-token" content="@csrf">
    <link rel="stylesheet" href="{!! asset('lucid/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') !!}">
@endsection

@section('scripts')
    <script src="{!! asset('lucid/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js') !!}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/app.js') }}"></script>

    <script>
        var vm = new Vue({
            el: "#app_vue",
            data:  {
                total_utama: 1,
                datas: [],
                idnya: {!! json_encode($model->id) !!},
                f_id: '0', f_nama_survey: '', f_month: '', f_year: '', f_nama_petugas: '',
                f_hp_petugas: '',f_pendapatan: '' ,f_pengeluaran: '', f_jumlah_pegawai: '',
                f_pemberi_jawaban: '',f_kegiatan_utama: '' ,f_produk_utama: '', f_status_usaha: '',
                f_periode_pencacahan: '',

                fk_id: '0', fk_nama_pengantar: '', fk_nama_penerima: '', fk_tanggal_diserahkan: '', 
                fk_tanggal_dikembalikan: '', fk_status_dokumen: 2, fk_keterangan: '',

                list_survey: {!! json_encode($model->ListPeriodeSurvey) !!},
                list_kategori: {!! json_encode($model->ListKategori) !!},
            },
            computed: {
                pathname: function () {
                    if(this.idnya)
                        return (window.location.pathname).replace("/"+this.idnya+"/detail", "");
                    else
                        return (window.location.pathname).replace("/create", "");
                },
            },
            methods: {
                moneyFormat:function(amount){
                    var decimalCount = 0;
                    var decimal = ".";
                    var thousands = ",";
                    decimalCount = Math.abs(decimalCount);
                    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                    const negativeSign = amount < 0 ? "-" : "";

                    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                    let j = (i.length > 3) ? i.length % 3 : 0;

                    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");      
                },
                setDatas: function(){
                    var self = this;
                    console.log(self.pathname+"/data_survey");
                    $('#wait_progres').modal('show');
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    
                    $.ajax({
                        url : self.pathname+"/data_survey",
                        method : 'post',
                        dataType: 'json',
                        data:{
                            idnya: self.idnya,
                        },
                    }).done(function (data) {
                        self.datas = data.datas;
                        console.log(self.datas);
                        $('#wait_progres').modal('hide');
                    }).fail(function (msg) {
                        console.log(JSON.stringify(msg));
                        $('#wait_progres').modal('hide');
                    });
                },
                is_delete: function(params){
                    if(isNaN(params)) return false;
                    else return true;
                }, 
                buatKunjungan: function (event) {
                    var self = this;
                    if (event) {
                        self.f_id = event.currentTarget.getAttribute('data-survey');
                    }
                },
                updateKunjungan: function (event) {
                    var self = this;

                    if (event) {
                        self.fk_id = event.currentTarget.getAttribute('data-id');
                        self.fk_nama_pengantar = event.currentTarget.getAttribute('data-nama_pengantar');
                        self.fk_nama_penerima = event.currentTarget.getAttribute('data-nama_penerima');
                        self.fk_tanggal_diserahkan = event.currentTarget.getAttribute('data-tanggal_diserahkan');
                        $('#tanggal_diserahkan').val(self.fk_tanggal_diserahkan);
                        
                        self.fk_tanggal_dikembalikan = event.currentTarget.getAttribute('data-tanggal_dikembalikan');
                        $('#tanggal_dikembalikan').val(self.fk_tanggal_dikembalikan);
                        
                        self.fk_status_dokumen = event.currentTarget.getAttribute('data-status_dokumen');
                        self.fk_keterangan = event.currentTarget.getAttribute('data-keterangan');
                    }
                },
                saveKunjungan: function(){
                    var self = this;
                    
                    if(self.fk_tanggal_diserahkan.length==0 || self.fk_status_dokumen.length==0){
                        alert("Pastikan nama survey, tahun, bulan, nama petugas dan hp petugas telah diisi");
                    }
                    else{
                        $('#wait_progres').modal('show');
                        $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')} })

                        $.ajax({
                            url :  self.pathname + '/store_kunjungan',
                            method : 'post',
                            dataType: 'json',
                            data:{
                                perusahaan_survey_id: self.f_id,
                                id: self.fk_id,
                                nama_pengantar: self.fk_nama_pengantar,
                                nama_penerima: self.fk_nama_penerima,
                                tanggal_diserahkan: self.fk_tanggal_diserahkan, 
                                tanggal_dikembalikan: self.fk_tanggal_dikembalikan, 
                                status_dokumen: self.fk_status_dokumen,
                                keterangan: self.fk_keterangan,
                            },
                        }).done(function (data) {
                            self.f_id = '';
                            self.fk_id = '';
                            self.fk_nama_pengantar = '';
                            self.fk_nama_penerima = '';
                            self.fk_tanggal_diserahkan = '';
                            self.fk_tanggal_dikembalikan = '';
                            self.fk_status_dokumen = '';
                            self.fk_keterangan = '';
                            $('#form_kunjungan').modal('hide');
                            self.setDatas();
                        }).fail(function (msg) {
                            console.log(JSON.stringify(msg));
                            $('#wait_progres').modal('hide');
                        });
                    }
                },

                updateSurvey: function (event) {
                    var self = this;

                    if (event) {
                        self.f_id = event.currentTarget.getAttribute('data-id');
                        self.f_nama_survey = event.currentTarget.getAttribute('data-nama_survey');
                        self.f_month = event.currentTarget.getAttribute('data-month');
                        self.f_year = event.currentTarget.getAttribute('data-year');
                        self.f_nama_petugas = event.currentTarget.getAttribute('data-nama_petugas');
                        self.f_hp_petugas = event.currentTarget.getAttribute('data-hp_petugas');
                        self.f_pendapatan = event.currentTarget.getAttribute('data-pendapatan');
                        self.f_pengeluaran = event.currentTarget.getAttribute('data-pengeluaran');
                        self.f_jumlah_pegawai = event.currentTarget.getAttribute('data-jumlah_pegawai');
                        self.f_pemberi_jawaban = event.currentTarget.getAttribute('data-pemberi_jawaban');
                        self.f_kegiatan_utama = event.currentTarget.getAttribute('data-kegiatan_utama');
                        self.f_produk_utama = event.currentTarget.getAttribute('data-produk_utama');
                        self.f_status_usaha = event.currentTarget.getAttribute('data-status_usaha');
                        self.f_periode_pencacahan = event.currentTarget.getAttribute('data-periode_pencacahan');
                    }
                },
                saveSurvey: function(){
                    var self = this;
                    
                    if(self.f_nama_survey.length==0 || self.f_year.length==0 || 
                        self.f_nama_petugas.length==0 || self.f_hp_petugas.length==0){
                        alert("Pastikan nama survey, tahun, bulan, nama petugas dan hp petugas telah diisi");
                    }
                    else{
                        
                        if(isNaN(self.f_jumlah_pegawai) || isNaN(self.f_pendapatan) || isNaN(self.f_pengeluaran) || 
                            isNaN(self.f_month) || isNaN(self.f_year)){
                            alert("Isian jumlah pegawai, bulan, tahun, pendapatan atau pengeluaran harus angka");
                        }
                        else{
                            $('#wait_progres').modal('show');
                            $.ajaxSetup({ headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')} })

                            $.ajax({
                                url :  self.pathname + '/store_survey',
                                method : 'post',
                                dataType: 'json',
                                data:{
                                    perusahaan_id: self.idnya,
                                    id: self.f_id,
                                    nama_survey: self.f_nama_survey,
                                    month: self.f_month,
                                    year: self.f_year, 
                                    nama_petugas: self.f_nama_petugas, 
                                    hp_petugas: self.f_hp_petugas,
                                    pendapatan: self.f_pendapatan,
                                    pengeluaran: self.f_pengeluaran,
                                    jumlah_pegawai: self.f_jumlah_pegawai,
                                    pemberi_jawaban: self.f_pemberi_jawaban,
                                    kegiatan_utama: self.f_kegiatan_utama,
                                    produk_utama: self.f_produk_utama,
                                    status_usaha: self.f_status_usaha,
                                    periode_pencacahan: self.f_periode_pencacahan,
                                },
                            }).done(function (data) {
                                self.f_id = '';
                                self.f_nama_survey = '';
                                self.f_month = '';
                                self.f_year = '';
                                self.f_nama_petugas = '';
                                self.f_hp_petugas = '';
                                self.f_pendapatan = '';
                                self.f_pengeluaran = '';
                                self.f_jumlah_pegawai = '';
                                self.f_pemberi_jawaban = '';
                                self.f_kegiatan_utama = '';
                                self.f_produk_utama = '';
                                self.f_status_usaha = '';
                                self.f_periode_pencacahan = '';
                                $('#form_survey').modal('hide');
                                self.setDatas();
                            }).fail(function (msg) {
                                console.log(JSON.stringify(msg));
                                $('#wait_progres').modal('hide');
                            });
                        }
                    }
                },
                delData: function (idnya) {
                    var self = this;
                    if(confirm("Anda yakin ingin menghapus data ini?")){
                        $('#wait_progres').modal('show');
                        $.ajax({
                            url : self.pathname+"/" + idnya + "/destroy_survey",
                            method : 'get',
                            dataType: 'json',
                        }).done(function (data) {
                            window.location.reload(true);
                            $('#wait_progres').modal('hide');
                        }).fail(function (msg) {
                            console.log(JSON.stringify(msg));
                            $('#wait_progres').modal('hide');
                        });
                    }
                },
                delDataTemp: function (index) {
                    var self = this;
                    if(confirm("Anda yakin ingin menghapus data ini?")){
                        $('#wait_progres').modal('show');
                        self.rincian.splice(index, 1);
                        $('#wait_progres').modal('hide');
                    }
                },
            }
        });

        $(document).ready(function() {
            vm.setDatas();
            
            $('.datepicker').datepicker({
                endDate: 'd',
            });
        });
        
        $('#tanggal_dikembalikan').change(function() {
            vm.fk_tanggal_dikembalikan = this.value;
        });
        
        $('#tanggal_diserahkan').change(function() {
            vm.fk_tanggal_diserahkan = this.value;
        });
    </script>
@endsection
