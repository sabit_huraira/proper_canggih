<b>Daftar Survey</b> -
&nbsp <a href="#" id="add-utama" data-toggle="modal" data-target="#form_survey">Tambah Survey &nbsp &nbsp<i class="icon-plus text-info"></i></a>
<br/>
<small class="text-muted font-italic font-weight-lighter float-left">*Isian kunjungan survey dapat diisi pada "detail" data perusahaan.</small>

<div class="table-responsive">
    <table class="table m-b-0 table-bordered">
        <thead>
            <tr>
                <th>No</th>
                <th class="text-center">Nama Survey</th>
                <th class="text-center">Periode</th>
                <th class="text-center">Tahun</th>    
                <th class="text-center">Nama Petugas</th>    
                <th class="text-center">Keterangan</th>   
            </tr>
        </thead>

        <tbody>
            <tr v-for="(data, index) in datas" :key="data.id">
                <td>
                    <template v-if="is_delete(data.id)">
                        <a :data-id="data.id" v-on:click="delData(data.id)"><i class="fa fa-trash text-danger"></i>&nbsp </a>
                    </template>
                    
                    <template v-if="!is_delete(data.id)">
                        <a :data-id="data.id" v-on:click="delDataTemp(index)"><i class="fa fa-trash text-danger"></i>&nbsp </a>
                    </template>

                    <a href="#" role="button" v-on:click="updateSurvey" data-toggle="modal" 
                            :data-id="data.id" :data-nama_survey="data.nama_survey" 
                            :data-month="data.month" :data-year="data.year" 
                            :data-nama_petugas="data.nama_petugas" :data-hp_petugas="data.hp_petugas" 
                            :data-pendapatan="data.pendapatan" :data-pengeluaran="data.pengeluaran" 
                            :data-jumlah_pegawai="data.jumlah_pegawai" :data-pemberi_jawaban="data.pemberi_jawaban" 
                            :data-kegiatan_utama="data.kegiatan_utama" :data-produk_utama="data.produk_utama" 
                            :data-status_usaha="data.status_usaha" :data-periode_pencacahan="data.periode_pencacahan" 
                            :data-index="index" 
                            data-target="#form_survey"> <i class="icon-pencil"></i></a>
                    @{{ index+1 }}
                </td>

                <td>@{{ data.nama_survey }} (pemberi jawaban: @{{ data.pemberi_jawaban }} )
                    <input type="hidden" :name="'u_nama_survey'+data.id" v-model="data.nama_survey">
                    <input type="hidden" :name="'u_pemberi_jawaban'+data.id" v-model="data.pemberi_jawaban"></td>
                <td>
                    Periode Pencacahan: @{{ list_survey[data.periode_pencacahan] }} <br/>
                    
                    <div v-if="data.periode_pencacahan != 4 && data.periode_pencacahan != 0"> 
                        <div v-if="data.periode_pencacahan == 1">Bulan: @{{ data.month }} <br/></div>
                        <div v-else-if="data.periode_pencacahan == 2">Triwulan: @{{ data.month }} <br/></div>
                        <div v-else-if="data.periode_pencacahan == 3">Semester: @{{ data.month }} <br/></div>   
                    </div>

                    <input type="hidden" :name="'u_month'+data.id" v-model="data.month">
                    <input type="hidden" :name="'u_periode_pencacahan'+data.id" v-model="data.periode_pencacahan">
                </td>
                <td>@{{ data.year }}<input type="hidden" :name="'u_year'+data.id" v-model="data.year"></td>
                <td>@{{ data.nama_petugas }} (@{{ data.hp_petugas }})
                    <input type="hidden" :name="'u_nama_petugas'+data.id" v-model="data.nama_petugas">
                    <input type="hidden" :name="'u_hp_petugas'+data.id" v-model="data.hp_petugas">
                </td>
                <td>
                    Kegiatan Utama: @{{ data.kegiatan_utama }} <br/>
                    Produk Utama: @{{ data.produk_utama }} <br/>
                    Status Usaha: @{{ (data.status_usaha==1 ? "Aktif" : "Tidak Aktif") }} <br/>
                    Pendapatan: Rp. @{{ moneyFormat(data.pendapatan) }} <br/>
                    Pengeluaran: Rp. @{{ moneyFormat(data.pengeluaran) }} <br/>
                    Jumlah Pegawai: @{{ data.jumlah_pegawai }} 

                    <input type="hidden" :name="'u_kegiatan_utama'+data.id" v-model="data.kegiatan_utama">
                    <input type="hidden" :name="'u_produk_utama'+data.id" v-model="data.produk_utama">
                    <input type="hidden" :name="'u_status_usaha'+data.id" v-model="data.status_usaha">

                    <input type="hidden" :name="'u_pendapatan'+data.id" v-model="data.pendapatan">
                    <input type="hidden" :name="'u_pengeluaran'+data.id" v-model="data.pengeluaran">
                    <input type="hidden" :name="'u_jumlah_pegawai'+data.id" v-model="data.jumlah_pegawai">
                </td>  
            </tr>
        </tbody>
    </table>
</div>